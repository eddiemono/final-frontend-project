import React from 'react';
import {useDispatch, useSelector,  } from 'react-redux';
import { Link } from 'react-router-dom';
import './Users.css'
import RecordDrop from './dropDown/RecordDrop';
import { deletetype } from './reducers/typeReducer';

const Type = () => {
  const types = useSelector((state) => state.types.types)
  console.log(types);
const dispatch = useDispatch()

const handleDelete = (id) => {
dispatch(deletetype({id: id}))
}

  return (
    <>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
    {/* <Link to='/engine'>Record Engine</Link> */}

<RecordDrop/>
    {/* <Link to='/parking'>Parking Space</Link> */}
    {/* <Link to='/service'>Services</Link> */}
    {/* <Link to='/users'>Users</Link> */}
    <Link to='/'>Sign out</Link>
    </div>

    <div className='container'>
    <h3>Type Description</h3>
    <Link to='/typeCreate' className='btn btn-success my-3'>Create +</Link>
    <table className='table'>
      <thead>
        <tr>
          <th>ID</th>
          <th>DESCRIPTION</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
{types.map((type, index) => {
  return(
    <tr key={index}>
  <td>{type.id}</td>
  
  <td>{type.description}</td>
  <td>

<Link to={`/edit/${type.id}`} className='btn btn-sm btn-primary'>Edit</Link>
<button onClick={() => handleDelete(type.id)} className='btn btn-sm btn-danger'>Delete</button>
</td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>

      
    </div>
        </>

  );
}

export default Type;
