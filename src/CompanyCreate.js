import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { addCompany } from './reducers/companyReducer';
import { useNavigate } from 'react-router-dom';

const CompanyCreate = () => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [address, setAddress] = useState('')
  const [telephone, setTelephone] = useState('')
  const [photo, setPhoto] = useState('')

const companies = useSelector((state) => state.companies.companies)
console.log(companies)
const dispatch = useDispatch()
const Navigate = useNavigate()

const createUser = (e) => {
e.preventDefault()
dispatch(addCompany({id: Date.now(), name, email, address, telephone, photo }))
Navigate('/company')
}


return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border bg-secondary text-white p-5'>
    <h1>Add A User</h1>
    <form onSubmit={createUser}>
      <div>
        <label htmlFor='name'>Name:</label>
        <input type='text' name='name' className='form-control' placeholder='Enter Name' 
        onChange={e => setName(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='address'>Address:</label>
        <input type='text' name='address' className='form-control' placeholder='Enter Address' 
        onChange={e => setAddress(e.target.value)}/>
      </div>

      <div>
      <label htmlFor='email'>Email:</label>
        <input type='text' name='email' className='form-control' placeholder='Enter Email' 
        onChange={e => setEmail(e.target.value)}/>
      </div>

      <div>
        <label htmlFor='telephone'>Telephone:</label>
        <input type='phone' name='telephone' className='form-control' placeholder='Enter Telephone' 
        onChange={e => setTelephone(e.target.value)}/>
      </div>

      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Upload Photo' 
        onChange={e => setPhoto(e.target.value)}/>
      </div><br/>
      <button className='btn btn-info'>Submit</button>
    </form>
    </div>
    </div>
  );
}

export default CompanyCreate;
