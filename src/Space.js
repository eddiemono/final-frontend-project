import React from 'react';
import {useDispatch, useSelector,  } from 'react-redux';
import { Link } from 'react-router-dom';
import './Users.css'
import RecordDrop from './dropDown/RecordDrop';
import { deleteSpace } from './reducers/spaceReducer';

const Space = () => {
  const spaces = useSelector((state) => state.spaces.spaces)
  console.log(spaces);
const dispatch = useDispatch()

const handleDelete = (id) => {
dispatch(deleteSpace({id: id}))
}

  return (
    <>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
    {/* <Link to='/engine'>Record Engine</Link> */}

<RecordDrop/>
    {/* <Link to='/parking'>Parking Space</Link> */}
    {/* <Link to='/service'>Services</Link> */}
    {/* <Link to='/users'>Users</Link> */}
    <Link to='/'>Sign out</Link>
    </div>

    <div className='container'>
    <h3>Space Information</h3>
    <Link to='/spaceCreate' className='btn btn-success my-3'>Record +</Link>
    <table className='table'>
      <thead>
        <tr>
          <th>ID</th>
          <th>DESCRIPTION</th>
          <th>LOCALIZATION</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
{spaces.map((space, index) => {
  return(
    <tr key={index}>
  <td>{space.id}</td>
  <td>{space.description}</td>
  <td>{space.localization}</td>
  <td>{space.photo}</td>


  <td>

<Link to={`/edit/${space.id}`} className='btn btn-sm btn-primary'>Edit</Link>
<button onClick={() => handleDelete(space.id)} className='btn btn-sm btn-danger'>Delete</button>
</td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>

      
    </div>
        </>

  );
}

export default Space;
